//
//  header.h
//  challenge#6ISNELabII
//
//  Created by Panpech Pothong on 10/19/2559 BE.
//  Copyright © 2559 Panpech Pothong. All rights reserved.
//

#ifndef Graph
#include <queue>
#include <stack>
#include <limits>
#include <iomanip>
using namespace std;

template<class T> class Graph;

template<class T>
class Node {
public:
    Node() { left = right = NULL; }
    Node(const T& el, Node *l = 0, Node *r = 0) {
        key = el; left = l; right = r;
    }
    T key;
    Node *left, *right;
};

template<class T>
class Graph {
public:
    Graph() { root = 0; }
    ~Graph() { clear(); }
    void clear() { clear(root); root = 0; }
    bool isEmpty() { return root == 0; }
    void inorder() { inorder(root); }
    void insert(const T& el,int row);
    void deleteNode(Node<T> *& node);
    void visit(Node<T> *p);
    void start(int num);
    bool check_multi();
    bool check_pseudo();
    bool check_di();
    void shortest_part(int size);
    void display();
    
protected:
    Node<T> *root;
    
    void clear(Node<T> *p);
    void inorder(Node<T> *p);
    
};

template<class T>
void Graph<T>::clear(Node<T> *p)
{
    if (p != 0) {
        clear(p->left);
        clear(p->right);
        delete p;
    }
}

template<class T>
void Graph<T>::inorder(Node<T> *p) {
    
    if (root != NULL) {
        inorder(p->left);
        visit(p);
        inorder(p->right);
        
    }
    else {
        cout << " The tree is empty" << endl;
    }
    
}
template<class T>
void Graph<T>::insert(const T &el,int row) {
    Node<T> *p = root, *prev = 0,*temp=root;
    for (int i = 1; i < row; i++)
    {
        temp = temp->left;
    }
    p = temp;
    while (p != 0) {
        prev = p;
        p = p->right;
    }
    if (root == 0)
        root = new Node<T>(el);
    else
        prev->right = new Node<T>(el);
}

template<class T>
void Graph<T>::deleteNode(Node<T> *&node) {
    Node<T> *prev, *tmp = node;
    if (node->right == 0)
        node = node->left;
    else if (node->left == 0)
        node = node->right;
    else {
        tmp = node->left;
        prev = node;
        while (tmp->right != 0) {
            prev = tmp;
            tmp = tmp->right;
        }
        node->key = tmp->key;
        if (prev == node)
            prev->left = tmp->left;
        else prev->right = tmp->left;
    }
    delete tmp;
}
template<class T>
void Graph<T>::visit(Node<T> *p)
{
    cout << p->key << endl;
}
template<class T>
inline void Graph<T>::start(int num)
{
    for (int i = 0; i < num; i++)
    {
        Node<T> *p = root, *prev = 0;
        while (p != 0)
        {
            prev = p;
            p = p->left;
        }
        if (root == 0)
        {
            root = new Node<T>(i+1);
        }
        else
            prev->left = new Node<T>(i+1);
    }
    
    
}
template<class T>
inline bool Graph<T>::check_multi()
{
    int  hold1 = 0, hold2 = 0;
    Node<T> *p = root,*temp = root;
    while (p != 0)
    {
        temp = p->right;
        
        while (temp->right != 0)
        {
            
            hold1 = temp->key;
            hold2 = temp->right->key;
            if (hold1/1000 == hold2/1000)
            {
                return true;
            }
            else
            {
                temp = temp->right;
            }
        }
        p = p->left;
    }
    
    return false;
}
template<class T>
inline bool Graph<T>::check_pseudo()
{
    T  hold1 = 0, hold2 = 0;
    Node<T> *p = root, *temp = root;
    while (p != 0)
    {
        hold1 = p->key;
        hold1 = hold1;
        temp = p->right;
        while (temp != 0)
        {
            hold2 = temp->key;
            
            if (hold1 == hold2/1000)
            {
                return true;
            }
            else
            {
                temp = temp->right;
            }
        }
        p = p->left;
    }
    
    return false;
}
template<class T>
inline bool Graph<T>::check_di()
{
    int hold1, hold2;
    Node<T> *p = root;
    hold1 = p->right->key;
    p = p->left;
    hold2 = p->right->key;
    if (hold1 % 1000 == hold2 % 1000)
    {
        return true;
    }
    else
        return false;
}
template<class T>
inline void Graph<T>::shortest_part(int size)
{
    Node<T> *p = root, *temp = root;
    int weight = 0, index = 1, min = 0;
    int** ary = new int*[size + 1];
    for (int i = 0; i < size + 1; ++i)
        ary[i] = new int[size + 1];
    queue<int>path;
    path.push(1);
    for (int q = 1; q < size + 1; q++)
    {
        ary[1][q] = 10000;
    }
    for (int round = 1; round < size + 1; round++)
    {
        p = root;
        temp = root;
        for (int x = 1; x < size + 1; x++)
        {
            if (ary[x][round] != 10000)
            {
                ary[x][round] = 99999;
            }
        }
        for (int i = 1; i < index; i++)
        {
            p = p->left;
        }
        
        temp = p->right;
        while (temp != 0)
        {
            if (round != 1)
            {
                if (ary[temp->key / 1000][round] != 10000)
                {
                    if (ary[temp->key / 1000][round - 1] < (temp->key % 1000) + weight)
                    {
                        ary[temp->key / 1000][round] = ary[temp->key / 1000][round - 1];
                    }
                    else
                    {
                        ary[temp->key / 1000][round] = (temp->key % 1000) + weight;
                    }
                }
            }
            else
            {
                ary[temp->key / 1000][round] = (temp->key % 1000) + weight;
            }
            temp = temp->right;
        }
        min = 99999;
        for (int t = 1; t < size + 1; t++)
        {
            if (ary[t][round] < min)
            {
                min = ary[t][round];
                index = t;
            }
        }
        weight = min;
        if (round < size)
        {
            path.push(index);
        }
        for (int o = round + 1; o < size + 1; o++)
        {
            ary[index][o] = 10000;
        }
    }
    cout << "DIJKSTRA'S Array" << endl << endl;
    cout << "     ";
    while (!path.empty())
    {
        cout << setw(7) << "[" << path.front() << "]";
        path.pop();
    }
    cout << endl;
    for (int y = 1; y < size + 1; y++)
    {
        cout << "[" << y << "]";
        for (int x = 1; x < size + 1; x++)
        {
            if (ary[y][x] == 10000)
            {
                cout << "          ";
            }
            else if (ary[y][x] == 99999)
            {
                cout << setw(9) << "X";
            }
            else
                cout << setw(9) << ary[y][x];
        }
        cout << endl;
    }
    cout << endl << endl;
    

}
template<class T>
void Graph<T>::display() {
    int hold1 = 0, hold2 = 0;
    Node<T> *p = root, *temp = root;
    cout << "GRAPH [x] n" << endl;
    cout<< " x is node & n is weight"<<endl;
    while (p != 0)
    {
        temp = p->right;
        hold1 = p->key;
        cout << "[" << hold1 << "] =>  ";
        while (temp != 0)
        {
            hold2 = temp->key;
            cout << "[" << hold2 / 1000 << "] " << hold2 % 1000 << "  ";
            temp = temp->right;
            
        }
        cout << endl << endl;
        p = p->left;
    }
}
#endif
