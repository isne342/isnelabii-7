//
//  main.cpp
//  challenge#6ISNELabII
//
//  Created by Panpech Pothong on 10/12/2559 BE.
//  Copyright © 2559 Panpech Pothong. All rights reserved.
//
#include <iostream>
#include <list>
#include <queue>
#include <string>
#include "Header.h"
using namespace std;

void input(int &row, int &column)
{
    cout << "Insert numbers of nodes : ";
    while (cin >> row)
    {
        if (row >= 2)
        {
            break;
        }
        else
        {
            cout << "at least 2 nodes" << endl;
            cout << "Insert numbers of nodes : ";
        }
    }
    column = row;
}
void check(Graph<int> &graph, bool &check_weight, bool &check_complete)
{
    graph.display();
    if (graph.check_multi())
    {
        cout << "This is MutiGraph" << endl;
    }
    
    if (graph.check_pseudo())
    {
        cout << "This is PsudoGraph" << endl;
    }
    if(check_weight)
    {
        cout << "This is Weighted Graph" << endl;
    }
    
    if (graph.check_di())
    {
        cout << "This is Directed Graph" << endl;
    }
    
    if (check_complete)
    {
        cout << "This isComplete Graph" << endl;
    }
    
}
int main()
{
    Graph<int> graph;
    int row = 0, column = 0, hold = 0;
    input(row, column);
    
    bool check_weight = false, check_complete = true;
    queue<int> arr_hold;
    int** ary = new int*[row];
    for (int i = 0; i < row; ++i)
        ary[i] = new int[column];
    
    for (int r = 0; r < row; r++)
    {
        for (int c = 0; c < column; c++)
        {
            cout << "Array[" << r << "][" << c << "] = ";
            cin >> hold;
            ary[r][c] = hold;
        }
    }
    cout << endl << endl;
    graph.start(row);
    for (int y = 0; y < row; y++)
    {
        for (int x = 0; x < column; x++)
        {
            if (ary[y][x] != 0)
            {
                graph.insert(ary[y][x] + ((x + 1) * 1000), y + 1);
                check_weight = true;
            }
            else { check_complete = false; }
        }
    }
    check(graph, check_weight, check_complete);
    graph.shortest_part(row);
    return 0;
}



